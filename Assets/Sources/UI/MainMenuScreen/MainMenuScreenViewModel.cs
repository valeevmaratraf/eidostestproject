using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EidosTestProject
{
    public class MainMenuScreenViewModel : MonoBehaviour, IViewModel
    {
        [SerializeField] private MainMenuScreenView view;

        [SerializeField] private GameObject loadNotesScreen;
        [SerializeField] private GameObject createNoteScreen;
        [SerializeField] private GameObject settingsScreen;

        private MainMenuScreenModel model;

        private void Awake()
        {
            model = new MainMenuScreenModel();

            view.Init(this);

            view.LoadNotesButtonClicked += HandleLoadNotesButtonClick;
            view.CreateNoteButtonClicked += HandleCreateNoteButtonClick;
            view.SettingsNoteButtonClicked += HandleSettingsButtonClick;
        }

        private void HandleCreateNoteButtonClick()
        {
            createNoteScreen.SetActive(true);
        }

        private void HandleLoadNotesButtonClick()
        {
            loadNotesScreen.SetActive(true);
        }

        private void HandleSettingsButtonClick()
        {
            settingsScreen.SetActive(true);
        }
    }
}
