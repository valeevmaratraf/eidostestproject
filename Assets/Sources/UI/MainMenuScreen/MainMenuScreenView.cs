using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EidosTestProject
{
    public class MainMenuScreenView : MonoBehaviour, IView
    {
        [SerializeField] private Button createNoteButton;
        [SerializeField] private Button loadNotesButton;
        [SerializeField] private Button settingsButton;

        private MainMenuScreenViewModel viewModel;

        public IViewModel ViewModel => viewModel;

        public event Action CreateNoteButtonClicked;
        public event Action LoadNotesButtonClicked;
        public event Action SettingsNoteButtonClicked;


        public void Init(IViewModel mainMenuViewModel)
        {
            viewModel = mainMenuViewModel as MainMenuScreenViewModel;

            createNoteButton.onClick.AddListener(() => CreateNoteButtonClicked?.Invoke());
            loadNotesButton.onClick.AddListener(() => LoadNotesButtonClicked?.Invoke());
            settingsButton.onClick.AddListener(() => SettingsNoteButtonClicked?.Invoke());
        }
    }
}
