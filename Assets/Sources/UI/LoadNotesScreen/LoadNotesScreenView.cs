using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EidosTestProject
{
    public class LoadNotesScreenView : MonoBehaviour, IView
    {
        [SerializeField] private GameObject noteRowPrefab;
        [SerializeField] private Transform listRoot;
        [SerializeField] private Button closeButton;

        private LoadNotesScreenViewModel viewModel;
        private List<NoteRowViewModel> availableRows;

        public IViewModel ViewModel => viewModel;
        public event Action CloseButtonClicked;

        public void Init(IViewModel viewModel)
        {
            this.viewModel = viewModel as LoadNotesScreenViewModel;

            this.viewModel.NotesListChanged += HandleNotesListChange;

            availableRows = new List<NoteRowViewModel>(8);
            listRoot.GetComponentsInChildren(true, availableRows);

            closeButton.onClick.AddListener(() => CloseButtonClicked?.Invoke());
        }

        private void HandleNotesListChange(IReadOnlyList<string> notesList)
        {
            if (availableRows.Count < notesList.Count)
            {
                int availableRowsCount = availableRows.Count;
                for (int i = 0; i < notesList.Count - availableRowsCount; i++)
                {
                    CreateRow();
                }
            }
            else if (availableRows.Count > notesList.Count)
            {
                for (int i = notesList.Count - 1; i < availableRows.Count; i++)
                {
                    availableRows[i].gameObject.SetActive(false);
                }
            }

            for (int i = 0; i < notesList.Count; i++)
            {
                NoteRowViewModel row = availableRows[i];
                row.gameObject.SetActive(true);
                row.SetNote(notesList[i]);
            }
        }

        private void CreateRow()
        {
            var row = Instantiate(noteRowPrefab, listRoot);
            availableRows.Add(row.GetComponent<NoteRowViewModel>());
        }
    }
}
