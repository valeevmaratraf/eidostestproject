using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EidosTestProject
{
    public class LoadNotesScreenModel : IModel
    {
        private List<string> noteNames;

        public event PropertyChange<object> PropertyChanged;

        public IReadOnlyList<string> NotesList => noteNames.AsReadOnly();

        public LoadNotesScreenModel()
        {
            noteNames = new List<string>();
        }

        public void SetNoteNames(string[] noteNames)
        {
            this.noteNames.Clear();
            this.noteNames.AddRange(noteNames);
            PropertyChanged?.Invoke(nameof(NotesList), NotesList);
        }
    }
}
