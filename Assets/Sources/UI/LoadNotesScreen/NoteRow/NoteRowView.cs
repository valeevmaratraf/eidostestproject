using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace EidosTestProject
{
    public class NoteRowView : MonoBehaviour, IView
    {
        [SerializeField] private Button noteButton;
        [SerializeField] private TextMeshProUGUI noteName;

        private NoteRowViewModel noteRowModelView;

        public IViewModel ViewModel => noteRowModelView;

        public event Action NoteRowButtonClicked;

        public void Init(IViewModel viewModel)
        {
            noteRowModelView = viewModel as NoteRowViewModel;

            noteButton.onClick.AddListener(() => NoteRowButtonClicked?.Invoke());

            noteRowModelView.OnModelChanged += HandleRowModelPropertyChanged;
        }

        private void HandleRowModelPropertyChanged(NoteRowModel model)
        {
            if (model == null)
            {
                return;
            }

            gameObject.name = model.NoteName;
            noteName.text = model.NoteName;
        }
    }
}
