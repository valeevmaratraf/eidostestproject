using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EidosTestProject
{
    public class NoteRowModel : IModel
    {
        private string noteName;

        public string NoteName
        {
            get => noteName;
            set
            {
                if (noteName != value)
                {
                    noteName = value;
                    PropertyChanged?.Invoke(nameof(NoteName), noteName);
                }
            }
        }

        public NoteRowModel(string name)
        {
            noteName = name;
        }

        public event PropertyChange<object> PropertyChanged;
    }
}
