using System;
using UnityEngine;

namespace EidosTestProject
{
    public class NoteRowViewModel : MonoBehaviour, IViewModel
    {
        [SerializeField] private NoteRowView noteRowView;

        //TODO: use DI
        private NewNoteScreenViewModel newNoteScreenViewModel;

        private bool initialized;
        private NoteRowModel model;

        public event Action<NoteRowModel> OnModelChanged;

        private void Awake()
        {
            TryInitialize();
        }

        private void TryInitialize()
        {
            if (initialized)
            {
                return;
            }

            //TODO: use DI
            newNoteScreenViewModel = FindObjectOfType<NewNoteScreenViewModel>(true);

            model = new NoteRowModel("");

            noteRowView.Init(this);
            noteRowView.NoteRowButtonClicked += HandleNoteRowButtonClick;

            OnModelChanged(model);

            initialized = true;
        }

        private void HandleNoteRowButtonClick()
        {
            newNoteScreenViewModel.gameObject.SetActive(true);
            newNoteScreenViewModel.SetNote(model.NoteName);
        }

        public void SetNote(string noteName)
        {
            TryInitialize();
            model.NoteName = noteName;
            OnModelChanged?.Invoke(model);
        }
    }
}
