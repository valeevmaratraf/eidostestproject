using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EidosTestProject
{
    public class LoadNotesScreenViewModel : MonoBehaviour, IViewModel
    {
        [SerializeField] private LoadNotesScreenView view;

        private bool isInititalized;

        private LoadNotesScreenModel model;

        public event Action<IReadOnlyList<string>> NotesListChanged;

        private void OnEnable()
        {
            TryInitialize();

            var noteNames = Serializator.LoadFilesNames();
            model.SetNoteNames(noteNames);
        }

        private void TryInitialize()
        {
            if (isInititalized)
            {
                return;
            }

            model = new LoadNotesScreenModel();
            model.PropertyChanged += HandleModelPropertyChange;

            view.Init(this);
            view.CloseButtonClicked += HandleCloseButtonClick;

            isInititalized = true;
        }

        private void HandleModelPropertyChange(string property, object value)
        {
            if (property == nameof(model.NotesList))
            {
                NotesListChanged?.Invoke(model.NotesList);
            }
        }

        private void HandleCloseButtonClick()
        {
            gameObject.SetActive(false);
        }
    }
}
