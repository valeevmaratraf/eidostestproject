using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace EidosTestProject
{
    public class SettingsScreenView : MonoBehaviour, IView
    {
        [SerializeField] private Button closeButton;
        [SerializeField] private TMP_Dropdown languageDropdown;

        private bool isInitialized;
        private SettingsScreenViewModel viewModel;

        public IViewModel ViewModel => viewModel;

        public event Action<Language> LanguageDropdownValueChanged;
        public event Action CloseButtonClicked;

        public void Init(IViewModel viewModel)
        {
            if (isInitialized)
            {
                return;
            }

            this.viewModel = viewModel as SettingsScreenViewModel;
            this.viewModel.LanguageChanged += LanguageChangeHandler;
            isInitialized = true;

            closeButton.onClick.AddListener(() => CloseButtonClicked?.Invoke());
            languageDropdown.onValueChanged.AddListener((value) => LanguageDropdownValueChanged?.Invoke((Language)value));
        }


        private void LanguageChangeHandler(Language language)
        {
            var number = (int)language;
            if (languageDropdown.value != number)
            {
                languageDropdown.value = (int)language;
            }
        }
    }
}
