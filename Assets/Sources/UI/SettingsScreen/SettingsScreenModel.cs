using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EidosTestProject
{
    public class SettingsScreenModel : IModel
    {
        public event PropertyChange<object> PropertyChanged;

        private Language language;

        public Language Language
        {
            get => language;
            set
            {
                if (language != value)
                {
                    language = value;
                    PropertyChanged?.Invoke(nameof(Language), language);
                }
            }
        }
    }
}
