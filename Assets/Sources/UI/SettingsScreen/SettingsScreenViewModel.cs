using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EidosTestProject
{
    public class SettingsScreenViewModel : MonoBehaviour, IViewModel
    {
        [SerializeField] private SettingsScreenView view;

        private SettingsScreenModel model;

        public event Action<Language> LanguageChanged;

        private void Awake()
        {
            model = new SettingsScreenModel();

            model.PropertyChanged += ModelPropertyChangeHandler;

            view.Init(this);

            view.CloseButtonClicked += CloseButtonClickHandler;
            view.LanguageDropdownValueChanged += LanguageDropdownValueChangeHandler;
            model.Language = Localization.CurrentLanguage;
        }

        private void CloseButtonClickHandler()
        {
            gameObject.SetActive(false);
        }

        private void ModelPropertyChangeHandler(string prop, object value)
        {
            if (prop == nameof(model.Language))
            {
                LanguageChanged?.Invoke((Language)value);
            }
        }

        private void LanguageDropdownValueChangeHandler(Language language)
        {
            Localization.CurrentLanguage = language;

            //TODO: use DI instead load user settings again
            var userSettings = Serializator.LoadData<UserSettingsModel>(Constants.USER_SETTINGS_FILE_NAME);
            userSettings.SettedLanguage = language;
            Serializator.SaveData(userSettings, Constants.USER_SETTINGS_FILE_NAME);
        }
    }
}
