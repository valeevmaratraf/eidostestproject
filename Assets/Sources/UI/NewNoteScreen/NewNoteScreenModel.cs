using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EidosTestProject
{
    //TODO: rename to EditNoteScreenModel 
    public class NewNoteScreenModel : IModel
    {
        private NoteModel noteModel;

        public NoteModel NewNoteModel
        {
            get => noteModel;
            set
            {
                if (noteModel != value)
                {
                    noteModel = value;
                    PropertyChanged?.Invoke(nameof(NewNoteModel), noteModel);
                }
            }
        }

        public NewNoteScreenModel()
        {
            NewNoteModel = new NoteModel();
        }

        public event PropertyChange<object> PropertyChanged;
    }
}
