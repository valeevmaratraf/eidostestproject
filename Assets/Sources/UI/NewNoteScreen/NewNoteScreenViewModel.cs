using System;
using UnityEngine;

namespace EidosTestProject
{
    //TODO: rename to EditNoteScreenModel 
    public class NewNoteScreenViewModel : MonoBehaviour, IViewModel
    {
        [SerializeField] private NewNoteScreenView view;

        private NewNoteScreenModel model;
        public IModel Model => model;

        public event Action<NoteModel> NoteModelChanged;

        private void Awake()
        {
            model = new NewNoteScreenModel();
            view.Init(this);

            view.CloseButtonClicked += HandleCloseWindowButtonClick;
            view.SaveButtonClicked += HandleSaveNoteButtonClick;

            view.NoteNameChanged += HandleNoteNameInputFieldChange;
            view.NoteContentChanged += HandleNoteContentInputFieldChange;
        }

        private void HandleCloseWindowButtonClick()
        {
            gameObject.SetActive(false);
        }
        private void HandleSaveNoteButtonClick()
        {
            if (string.IsNullOrEmpty(model.NewNoteModel.Name))
            {
                return;
            }

            model.NewNoteModel.CreationDateTime = DateTime.Now;
            Serializator.SaveData(model.NewNoteModel, model.NewNoteModel.Name);
        }

        private void HandleNoteNameInputFieldChange(string value)
        {
            model.NewNoteModel.Name = value;
        }
        private void HandleNoteContentInputFieldChange(string value)
        {
            model.NewNoteModel.Content = value;
        }

        public void SetNote(string name)
        {
            var note = Serializator.LoadData<NoteModel>(name);
            model.NewNoteModel = note;
            NoteModelChanged?.Invoke(model.NewNoteModel);
        }
    }
}
