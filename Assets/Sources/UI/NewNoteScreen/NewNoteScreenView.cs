using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace EidosTestProject
{
    //TODO: rename to EditNoteScreenModel 
    public class NewNoteScreenView : MonoBehaviour, IView
    {
        [SerializeField] private TMP_InputField noteNameInputField;
        [SerializeField] private TMP_InputField noteContentInputField;

        [SerializeField] private Button saveNoteButton;
        [SerializeField] private Button closeWindowButton;

        private NewNoteScreenViewModel viewModel;
        public IViewModel ViewModel => viewModel;

        public event Action SaveButtonClicked;
        public event Action CloseButtonClicked;

        public event Action<string> NoteNameChanged;
        public event Action<string> NoteContentChanged;

        public void Init(IViewModel viewModel)
        {
            this.viewModel = viewModel as NewNoteScreenViewModel;

            saveNoteButton.onClick.AddListener(() => SaveButtonClicked?.Invoke());
            closeWindowButton.onClick.AddListener(() => CloseButtonClicked?.Invoke());

            noteNameInputField.onValueChanged.AddListener((value) => NoteNameChanged?.Invoke(value));
            noteContentInputField.onValueChanged.AddListener((value) => NoteContentChanged?.Invoke(value));

            this.viewModel.NoteModelChanged += HandleModelChange;
        }

        private void HandleModelChange(NoteModel model)
        {
            if (model == null)
            {
                noteContentInputField.text = "";
                noteNameInputField.text = "";
            }

            noteContentInputField.text = model.Content;
            noteNameInputField.text = model.Name;
        }
    }
}
