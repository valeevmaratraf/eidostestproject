using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EidosTestProject
{
    public interface IView
    {
        IViewModel ViewModel { get; }
        void Init(IViewModel viewModel);
    }
}
