using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

namespace EidosTestProject
{
    public interface IModel
    {
        event PropertyChange<object> PropertyChanged;
    }

    public delegate void PropertyChange<T>(string property, T value);
}