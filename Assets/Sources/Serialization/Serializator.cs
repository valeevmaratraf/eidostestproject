using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace EidosTestProject
{
    public static class Serializator
    {
        public static void SaveData(object data, string fileId)
        {
            var json = JsonConvert.SerializeObject(data);
            string filePath = Path.Combine(Application.persistentDataPath, fileId);
            File.WriteAllText($"{filePath}.json", json);
        }

        public static T LoadData<T>(string fileID)
        {
            string path = Path.Combine(Application.persistentDataPath, $"{fileID}.json");
            if (!File.Exists(path))
            {
                throw new FileNotFoundException();
            }

            var json = File.ReadAllText(path);
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static bool CheckForExist(string file)
        {
            string path = Path.Combine(Application.persistentDataPath, $"{file}.json");
            if (!File.Exists(path))
            {
                return false;
            }

            return true;
        }

        public static string[] LoadFilesNames(bool excludeUserSettings = true)
        {
            DirectoryInfo directory = new DirectoryInfo(Application.persistentDataPath);
            var files = directory.GetFiles("*.json");
            string[] names = new string[files.Length];
            for (int i = 0; i < files.Length; i++)
            {
                FileInfo file = files[i];
                names[i] = file.Name;
                names[i] = names[i].Replace(".json", "");
            }

            if (excludeUserSettings)
            {
                var namesList = new List<string>(names);
                namesList.Remove(Constants.USER_SETTINGS_FILE_NAME);
                return namesList.ToArray();
            }

            return names;
        }
    }
}
