using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace EidosTestProject
{
    public class LoadingProcess : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI loadingLable;

        private void Awake()
        {
            UserSettingsModel userSettings;

            if (!Serializator.CheckForExist(Constants.USER_SETTINGS_FILE_NAME))
            {
                userSettings = new UserSettingsModel();
                userSettings.SettedLanguage = Language.Russian;
                //TODO: make async
                Serializator.SaveData(userSettings, Constants.USER_SETTINGS_FILE_NAME);
            }
            else
            {
                //TODO: make async
                userSettings = Serializator.LoadData<UserSettingsModel>(Constants.USER_SETTINGS_FILE_NAME);
            }

            //TODO: make async
            Localization.Setup(userSettings.SettedLanguage);

            //TODO: make async
            LoadScene();

            loadingLable.GetComponent<TMP_TextLocalizer>().Localize();
        }

        private async void LoadScene()
        {
            //TODO: better use UniTask
            await Task.Delay(3000);
            SceneManager.LoadScene(1);
        }
    }
}
