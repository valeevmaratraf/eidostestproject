using Newtonsoft.Json;
using System;

namespace EidosTestProject
{
    public class NoteModel : IModel
    {
        [JsonProperty] private string name;
        [JsonProperty] private string content;
        [JsonProperty] private DateTime creationDateTime;

        [JsonIgnore]
        public string Name
        {
            get => name;
            set
            {
                if (value != name)
                {
                    name = value;
                    PropertyChanged?.Invoke(nameof(Name), name);
                }
            }
        }
        [JsonIgnore]
        public string Content
        {
            get => content;
            set
            {
                if (value != content)
                {
                    content = value;
                    PropertyChanged?.Invoke(nameof(Content), content);
                }
            }
        }
        [JsonIgnore]
        public DateTime CreationDateTime
        {
            get => creationDateTime;
            set
            {
                if (value != creationDateTime)
                {
                    creationDateTime = value;
                    PropertyChanged?.Invoke(nameof(CreationDateTime), creationDateTime);
                }
            }
        }

        public event PropertyChange<object> PropertyChanged;
    }
}
