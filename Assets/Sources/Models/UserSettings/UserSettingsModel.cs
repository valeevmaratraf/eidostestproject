using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EidosTestProject
{
    public class UserSettingsModel : IModel
    {
        [JsonProperty] private Language settedLanguage = Language.Russian;

        [JsonIgnore]
        public Language SettedLanguage
        {
            get => settedLanguage;
            set
            {
                if (value != settedLanguage)
                {
                    settedLanguage = value;
                    PropertyChanged?.Invoke(nameof(SettedLanguage), value);
                }
            }
        }

        public event PropertyChange<object> PropertyChanged;
    }
}
