using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace EidosTestProject
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TMP_TextLocalizer : MonoBehaviour
    {
        [SerializeField] private bool manualLocalize;
        [SerializeField] private string key;

        private TextMeshProUGUI text;

        private void Awake()
        {
            text = GetComponent<TextMeshProUGUI>();

            if (!manualLocalize)
            {
                Localize();
                Localization.LanguageChanged += (_) => Localize();
            }
        }

        public void Localize()
        {
            text.text = Localization.GetTranslation(key);
        }
    }
}
