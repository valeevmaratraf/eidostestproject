using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

namespace EidosTestProject
{
    public enum Language
    {
        English, Russian
    }

    public static class Localization
    {
        //Replace to scriptable object
        private const string GOOGLE_SPREADSHEET_KEY = "1_baGioexW2eXG7w2Q3VhgUpnBKj56ItQyScDbiOk9zs";

        private static Dictionary<string, string> ru;
        private static Dictionary<string, string> en;

        private static bool inited;
        private static Language currentLanguage;
        private static Dictionary<string, string> actualDictionary;

        public static event Action<Language> LanguageChanged;

        public static Language CurrentLanguage
        {
            get => currentLanguage;
            set
            {
                currentLanguage = value;
                actualDictionary = GetActualDictionary(value);
                LanguageChanged?.Invoke(value);
            }
        }

        static Localization()
        {
            ru = new Dictionary<string, string>(16);
            en = new Dictionary<string, string>(16);
        }

        //TODO: make async
        public static void Setup(Language language)
        {
            if (inited)
            {
                return;
            }

            var csv = DownloadKeys();
            SetLanguageDictionariesFromCsv(csv);

            CurrentLanguage = language;

            inited = true;
        }

        private static string DownloadKeys()
        {
            string url = @$"https://docs.google.com/spreadsheets/d/{GOOGLE_SPREADSHEET_KEY}/export?format=csv&id={GOOGLE_SPREADSHEET_KEY}&gid=0";

            WebClient wc = new WebClient();
            return wc.DownloadString(url);
        }

        private static void SetLanguageDictionariesFromCsv(string csv)
        {
            var rows = csv.Split('\n');
            for (int i = 1; i < rows.Length; i++)
            {
                string row = rows[i];
                var values = row.Split(',');

                ru.Add(values[0], values[1]);
                en.Add(values[0], values[2]);
            }
        }

        private static Dictionary<string, string> GetActualDictionary(Language language)
        {
            switch (language)
            {
                case Language.Russian: return ru;
                case Language.English: return en;
                default: throw new ArgumentException("Language is not found");
            };
        }

        public static string GetTranslation(string key)
        {
            if (!inited)
            {
                Debug.LogWarning("Trying get localization without setted language");
                Setup(Language.Russian);
            }

            return actualDictionary[key];
        }
    }
}
